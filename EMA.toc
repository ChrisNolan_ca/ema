## Interface: 80000
## Title: EMA
## Notes: Ebony's MultiBoxing Assistant
## Author: Jennifer Cally 'Ebony'
## Version: v8.0.1-Release(0089)
## SavedVariables: CoreProfileDB, CommunicationsProfileDB, TeamProfileDB, TagProfileDB, MessageProfileDB, CurrProfileDB, DisplayTeamProfileDB, FollowProfileDB, GuildProfileDB, InteractionProfileDB, ItemUseProfileDB, PurchaseProfileDB, QuestProfileDB,  QuestWatcherProfileDB, SellProfileDB, TalkProfileDB, ToonProfileDB, TradeProfileDB

#Libs
Embeds.xml
EbonyUtilities.lua


#Locales
Locales\Locales.xml

#GUI
GUI\EMAHelperSettings.lua

#Core
Core\Core.lua
Core\Module.lua
Core\Communications.lua

Core\Team.lua
Core\Tag.lua
Core\Message.lua

#Modules
Modules\Modules.xml